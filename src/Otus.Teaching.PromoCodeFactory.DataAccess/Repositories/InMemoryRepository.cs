﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateOneAsync(T entity)
        {
            Data.Add(entity);
            return Task.FromResult(entity);
        }

        public Task DeleteAsync(Guid id)
        {
            T entity = Data.FirstOrDefault(x=> x.Id == id);
            Data.Remove(entity);
            return Task.CompletedTask;
        }

        public Task<T> UpdateAsync(Guid id, T newEntity)
        {
            T entity= Data.FirstOrDefault(x => x.Id == id);
            if (entity != null)
            {
                entity = newEntity;
            }
            return Task.FromResult(entity); ;
        }
    }
}